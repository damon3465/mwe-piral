import * as React from "react";
import { PiletApi } from "app-shell";

import "./mystyle.scss";

export function setup(app: PiletApi) {
  app.registerTile(() => <div className="sample-div">Welcome to Piral!</div>, {
    initialColumns: 2,
    initialRows: 1,
  });
}
